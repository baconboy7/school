// Global app controller
import Search from './models/Search';
import Recipe from './models/Recipe';
import List from './models/List';
import Likes from './models/Likes';
import {elements, renderLoader, clearLoader} from "./views/base";
import * as searchView from "./views/searchView";
import * as recipeView from "./views/recipeView";
import * as listView from "./views/listView";
import * as likesView from "../../../forkify-final/src/js/views/likesView";



const state = {};

// const search = new Search('pizza');
// console.log(search);
// search.getResults();

const controlSearch = async () => {
  // get query from view
    const query = searchView.getInput();

    if (query){
        //new search object and add to state
        state.search = new Search(query);

        //Prep UI for results
        searchView.clearInput();
        searchView.clearResults();
        renderLoader(elements.searchResult);


        //search for recipes
        await state.search.getResults();

        //show results on UI
        console.log (state.search.result);
        searchView.renderResults(state.search.result);
        clearLoader();
    }
};

elements.searchForm.addEventListener('submit', e =>{
    e.preventDefault();
    controlSearch();
});

elements.searchResultPages.addEventListener('click', e =>{
    const btn = e.target.closest('.btn-inline');
    if (btn){
        const goToPage = parseInt(btn.dataset.goto, 10);
        // console.log(btn);
        // console.log(btn.dataset);
        searchView.clearResults();
        searchView.renderResults(state.search.result, goToPage);

    }
});


const controlRecipe = async () => {
    const id =window.location.hash.replace('#', '');

    if (id){
        //prep ui
        recipeView.clearRecipe();
        renderLoader(elements.recipe);

        //create recipe object
        state.recipe = new Recipe(id);

        //get data
        await state.recipe.getRecipe();
        state.recipe.parseIngredients();

        //servings and time
        state.recipe.calcServings();
        state.recipe.calcTime();
        //render
        clearLoader();
        recipeView.renderRecipe(state.recipe);
    }

};

//LIST CONTROLLER
const controlList = () => {
    //create list
    if (!state.list) state.list = new List();

    //add ingredients
    state.recipe.ingredients.forEach(el => {
       const item = state.list.addItem(el.count, el.unit, el.ingredient);
        listView.renderItem(item);
    });

};



window.addEventListener('hashchange', controlRecipe);
window.addEventListener('load', controlRecipe);

//servings updater
elements.recipe.addEventListener('click', e => {
   if (e.target.matches('.btn-decrease, .btn-decrease *')){
       if (state.recipe.servings > 1) {
           state.recipe.updateServings('dec');
           recipeView.updateServingsIngredients(state.recipe);
       }
   }

    if (e.target.matches('.btn-increase, .btn-increase *')){
        state.recipe.updateServings('inc');
        recipeView.updateServingsIngredients(state.recipe);
    }

    if(e.target.matches('.recipe__btn--add, .recipe__btn--add *')){
       controlList();
    }

});

const controlLike = () => {
    if (!state.likes) state.likes = new Likes();
    const currentID = state.recipe.id;

    // User has NOT yet liked current recipe
    if (!state.likes.isLiked(currentID)) {
        // Add like to the state
        const newLike = state.likes.addLike(
            currentID,
            state.recipe.title,
            state.recipe.author,
            state.recipe.img
        );
        // Toggle the like button
        likesView.toggleLikeBtn(true);

        // Add like to UI list
        likesView.renderLike(newLike);

        // User HAS liked current recipe
    } else {
        // Remove like from the state
        state.likes.deleteLike(currentID);

        // Toggle the like button
        likesView.toggleLikeBtn(false);

        // Remove like from UI list
        likesView.deleteLike(currentID);
    }
    likesView.toggleLikeMenu(state.likes.getNumLikes());
};

// Restore liked recipes on page load
window.addEventListener('load', () => {
    state.likes = new Likes();

    // Restore likes
    state.likes.readStorage();

    // Toggle like menu button
    likesView.toggleLikeMenu(state.likes.getNumLikes());

    // Render the existing likes
    state.likes.likes.forEach(like => likesView.renderLike(like));
});


// Handling recipe button clicks
elements.recipe.addEventListener('click', e => {
    if (e.target.matches('.btn-decrease, .btn-decrease *')) {
        // Decrease button is clicked
        if (state.recipe.servings > 1) {
            state.recipe.updateServings('dec');
            recipeView.updateServingsIngredients(state.recipe);
        }
    } else if (e.target.matches('.btn-increase, .btn-increase *')) {
        // Increase button is clicked
        state.recipe.updateServings('inc');
        recipeView.updateServingsIngredients(state.recipe);
    } else if (e.target.matches('.recipe__btn--add, .recipe__btn--add *')) {
        // Add ingredients to shopping list
        controlList();
    } else if (e.target.matches('.recipe__love, .recipe__love *')) {
        // Like controller
        controlLike();
    }
});
