import axios from 'axios';


export default class Recipe {
    constructor(id) {
        this.id = id;

    }

    async getRecipe(){
        try{
            const key = 'db91f275636600fd585ca8dba853515b';
            const proxy = 'https://cors-anywhere.herokuapp.com/';

            const res = await axios(`${proxy}http://food2fork.com/api/get?key=${key}&rId=${this.id}`);
            this.title = res.data.recipe.title;
            this.author = res.data.recipe.publisher;
            this.img = res.data.recipe.image_url;
            this.url = res.data.recipe.source_url;
            this.ingredients = res.data.recipe.ingredients;

        } catch (error){
            alert('Error');
        }
    }

    calcTime(){
        const numIng = this.ingredients.length;
        const periods = Math.ceil(numIng / 3);
        this.time = periods * 15;
    }

    calcServings() {
        this.servings = 4;
    }

    parseIngredients() {
        const unitsLong = ['tablespoons', 'tablespoon', 'ounces', 'ounce', 'teaspoons', 'teaspoon', 'cups', 'pounds'];
        const unitsShort = ['tbsp', 'tbsp', 'oz', 'oz', 'tsp', 'tsp', 'cup', 'pound'];
        const units = [...unitsShort, 'kg', 'g'];

        const newIngredients = this.ingredients.map(el => {
            // 1) Uniform units
            let ingredient = el.toLowerCase();
            unitsLong.forEach((unit, i) => {
                ingredient = ingredient.replace(unit, unitsShort[i]);
            });

            // 2) Remove parentheses
            ingredient = ingredient.replace(/ *\([^)]*\) */g, ' ');

            // 3) Parse ingredients into count, unit and ingredient
            const ingredientArray = ingredient.split(' ');
            const unitIndex = ingredientArray.findIndex(el2 => units.includes(el2));

            let ingredientObject;
            if (unitIndex > -1) {
                // There is a unit
                // Ex. 4 1/2 cups, arrCount is [4, 1/2] --> eval("4+1/2") --> 4.5
                // Ex. 4 cups, arrCount is [4]
                const arrCount = ingredientArray.slice(0, unitIndex);

                let count;
                if (arrCount.length === 1) {
                    count = eval(ingredientArray[0].replace('-', '+'));
                } else {
                    count = eval(ingredientArray.slice(0, unitIndex).join('+'));
                }

                ingredientObject = {
                    count,
                    unit: ingredientArray[unitIndex],
                    ingredient: ingredientArray.slice(unitIndex + 1).join(' ')
                };

            } else if (parseInt(ingredientArray[0], 10)) {
                // There is NO unit, but 1st element is number
                ingredientObject = {
                    count: parseInt(ingredientArray[0], 10),
                    unit: '',
                    ingredient: ingredientArray.slice(1).join(' ')
                }
            } else if (unitIndex === -1) {
                // There is NO unit and NO number in 1st position
                ingredientObject = {
                    count: 1,
                    unit: '',
                    ingredient
                }
            }

            return ingredientObject;
        });
        this.ingredients = newIngredients;
    }

    updateServings (type){
        //Servings
        const newServings = type === 'dec' ? this.servings - 1 : this.servings + 1;

        //ingredients
        this.ingredients.forEach(ingredient => {
            ingredient.count = ingredient.count * (newServings / this.servings);
        });

        this.servings = newServings;

    }
}