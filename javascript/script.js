// PROMISES

const getIDs = new Promise((resolve, reject) => {
    setTimeout(() => {
        const ids = [1, 2, 3, 4, 5];
        resolve(ids);
    }, 1500);
});

const getRecipie = id => {
    return new Promise((resolve, reject) => {
        setTimeout(id => {
            const recipie = {
                title: "Pasta",
                publisher: "Ryan"
            };
            resolve(`${id}: ${recipie.title}`);
        }, 1500, id)
    })
};

const getRelated = publisher => {
  return new Promise((resolve, reject) => {
      setTimeout(pub =>{
          const recipie2 = {Title:"other", publisher:"Ryan"}
          resolve(recipie2);
      }, 1500, publisher)
  })
};

getIDs.then(ids => {
    console.log(ids);
    return getRecipie(ids[2])
})
.then(recipie =>{
    console.log(recipie);
    return getRelated(recipie.publisher);
})
.then(recipie2 =>{
    console.log(recipie2);
});
