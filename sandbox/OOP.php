<?

//define class

class user {
  //properties
    
    private $name;
    
    private $age;
    
    
    //methods
    public function __construct($name, $age){
            $this->name = $name;
            $this->age = $age;
        }

    
    public function __get($property){
        if(property_exists($this, $property)){
            return $this->$property;
        }
    }
    
    public function __set($property, $value){
        if(property_exists($this, $property)){
             $this->$property = $value;
        }
        return $this;
    }
    
}


//instantiate user object

$user1 = new user('Ryan', 22);

$user1->__set('age', 23);

echo $user1->__get('age');
?>
