package com.ryan;

import java.util.Scanner;

public class Player {
    public String playerTurn() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter Rock, Paper, or Scissors:");
        String choice = scanner.next();
        choice = choice.toLowerCase();
        return choice;
    }
}
