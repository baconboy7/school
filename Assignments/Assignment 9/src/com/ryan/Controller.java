package com.ryan;

import java.util.Scanner;

public class Controller {

    public String determineWinner(String playerChoice, String computerChoice){

        if(playerChoice.equals(computerChoice)){
            return "tie";
        }

        if (playerChoice.equals("rock")) {
            if( computerChoice.equals("scissors")){
                return "player";
            } else{
                return "computer";
            }
        }

        if (playerChoice.equals("paper")) {
            if( computerChoice.equals("rock")){
                return "player";
            } else{
                return "computer";
            }
        }

        if (playerChoice.equals("scissors")) {
            if( computerChoice.equals("paper")){
                return "player";
            } else{
                return "computer";
            }
        }
        return null;
    }
    public int getRounds(){
        Scanner scanner = new Scanner(System.in);

        System.out.print("How many rounds of Rock, Paper, Scissors do you want to play:");
        int rounds = scanner.nextInt();

        if((rounds % 2) == 0){
            System.out.print("You must enter an odd number! Try again. ");
            getRounds();
        }
        return rounds;
    }

        public void playerWin(String playerChoice, String computerChoice) {
            System.out.println("You chose: " + playerChoice + ". The computer chose: " + computerChoice + ". You win!");
        }

        public void tie(String playerChoice, String computerChoice) {
            System.out.println("You chose: " + playerChoice + ". The computer chose: " + computerChoice + ". Its a tie.");
        }

        public void computerWin(String playerChoice, String computerChoice) {

            System.out.println("You chose: " + playerChoice + ". The computer chose: " + computerChoice + ". The computer wins.");
        }
}
