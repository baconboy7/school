package com.ryan;


public class Main {

    public static void main(String[] args) {
        Player player = new Player();
        Computer computer = new Computer();
        Controller controller = new Controller();

        int rounds = controller.getRounds();
        int playerWins = 0;
        int computerWins = 0;

        //for each round
        for (int i=0; i < rounds; i++) {
            //player turn
            String playerChoice = player.playerTurn();

            //computer turn
            String computerChoice = computer.computerTurn();

            //determine winner
            String winner = controller.determineWinner(playerChoice, computerChoice);

            //print winner
            if (winner.equals("player")){
                playerWins++;
                controller.playerWin(playerChoice, computerChoice);

            } else if (winner.equals("tie")){
                i--;
                controller.tie(playerChoice, computerChoice);
            }
            else if(winner.equals("computer")){
                controller.computerWin(playerChoice, computerChoice);
                computerWins++;
            }

        }
        //display totals
        System.out.println();
        System.out.println("Your victories: " + playerWins);
        System.out.println("Computer Victories: " + computerWins);

        //declare a winner
        if (playerWins > computerWins){
            System.out.println("You are the winner!");
        } else {
            System.out.println("You lose.");
        }

    }

}
