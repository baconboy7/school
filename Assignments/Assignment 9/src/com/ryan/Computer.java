package com.ryan;

import java.util.Random;

public class Computer {
    public String computerTurn() {
        Random r = new Random();
        int value = r.nextInt(3) + 1;
        String choice = convertToString(value);
        return choice;
    }

    private String convertToString(int value) {
        if (value == 1) {
            return "rock";
        }
        if (value == 2) {
            return "paper";
        }
        if (value == 3) {
            return "scissors";
        }
        return null;
    }
}
