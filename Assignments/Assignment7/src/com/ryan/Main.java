package com.ryan;

import java.awt.print.Book;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //get height in inches
        double height = getHeight();

        //get weight in pounds
        double weight = getWeight();

        //figure BMI
       double BMI = getBMI(height, weight);

        //print height, weight, and BMI
        System.out.println(" ");
        System.out.println("Height: " + height + " inches");
        System.out.println("Weight: " + weight + " pounds");
        System.out.println("BMI: " + BMI);


    }

    public static double getBMI(double height, double weight){
        double top = 704 * weight;
        double bottom = height * height;
        double BMI = top / bottom;
        return BMI;
    }

    public static double getHeight(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your height in inches:");
        String heightString = scanner.next();
        try {
            double height = Double.parseDouble(heightString);
            if (height > 0) {
                return height;

            } else{
                System.out.println("Please valid height!");
                getHeight();
            }

        } catch (NumberFormatException e) {
            System.out.println("Please enter a number!");
            getHeight();
        }
        return -1;

    }

    public static double getWeight(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your weight in pounds:");
        String heightString = scanner.next();
        try {
            double weight = Double.parseDouble(heightString);
            if (weight > 0) {
                return weight;

            } else{
                System.out.println("Please valid weight!");
                getWeight();
            }

        } catch (NumberFormatException e) {
            System.out.println("Please enter a number!");
            getWeight();
        }
        return -1;
    }
}
