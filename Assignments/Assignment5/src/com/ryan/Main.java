package com.ryan;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int rounds = getRounds();
        int playerWins = 0;
        int computerWins = 0;

        //for each round
        for (int i=0; i < rounds; i++) {
            //player turn
            String playerChoice = playerTurn();

            //computer turn
            String computerChoice = computerTurn();

            //determine winner
            String winner = determineWinner(playerChoice, computerChoice);
            if (winner.equals("player")){
                playerWins++;
                System.out.println("You chose: " + playerChoice + ". The computer chose: " + computerChoice + ". You win!" );


            } else if (winner.equals("tie")){
                System.out.println("You chose: " + playerChoice + ". The computer chose: " + computerChoice + ". It was a tie." );
                i--;
            }
            else if(winner.equals("computer")){
                System.out.println("You chose: " + playerChoice + ". The computer chose: " + computerChoice + ". The computer wins." );
                computerWins++;
            }

        }
        //display totals
        System.out.println("Your victories: " + playerWins);
        System.out.println("Computer Victories: " + computerWins);

        //declare a winner
        if (playerWins > computerWins){
            System.out.println("You are the winner!");
        } else {
            System.out.println("You lose.");
        }

    }


    public static String playerTurn() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter Rock, Paper, or Scissors:");
        String choice = scanner.next();
        choice = choice.toLowerCase();
        return choice;
    }

    public static String computerTurn() {
        Random r = new Random();
        int value = r.nextInt(3) + 1;
        String choice = convertToString(value);
        return choice;
    }

    public static String convertToString(int value) {
        if (value == 1) {
            return "rock";
        }
        if (value == 2) {
            return "paper";
        }
        if (value == 3) {
            return "scissors";
        }
        return null;
    }

    public static String determineWinner(String playerChoice, String computerChoice){

        if(playerChoice.equals(computerChoice)){
            return "tie";
        }

        if (playerChoice.equals("rock")) {
           if( computerChoice.equals("scissors")){
                return "player";
            } else{
               return "computer";
           }
        }

        if (playerChoice.equals("paper")) {
            if( computerChoice.equals("rock")){
                return "player";
            } else{
                return "computer";
            }
        }

        if (playerChoice.equals("scissors")) {
            if( computerChoice.equals("paper")){
                return "player";
            } else{
                return "computer";
            }
        }
        return null;
    }
    public static int getRounds(){
        Scanner scanner = new Scanner(System.in);

        System.out.print("How many rounds of Rock, Paper, Scissors do you want to play:");
        int rounds = scanner.nextInt();

        if((rounds % 2) == 0){
            System.out.print("You must enter an odd number! Try again. ");
            getRounds();
        }
        return rounds;
    }
}

