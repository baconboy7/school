package com.ryan;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //get number of dice rolls
        System.out.println("How many dice rolls should be simulated?");
        int numRolls = scanner.nextInt();
         int[]rolls = new int[numRolls];

        //simulate dice rolls
        for(int i = 0; i < numRolls; i++){
            rolls[i] = rollDie() + rollDie();
        }

        //get percents
        double percent2 = tally(2, rolls);
        double percent3 = tally(3, rolls);
        double percent4 = tally(4, rolls);
        double percent5 = tally(5, rolls);
        double percent6 = tally(6, rolls);
        double percent7 = tally(7, rolls);
        double percent8 = tally(8, rolls);
        double percent9 = tally(9, rolls);
        double percent10 = tally(10, rolls);
        double percent11 = tally(11, rolls);
        double percent12 = tally(12, rolls);

        //print
        System.out.println("Here are the results:");
        System.out.println("2: " + percent2 + "%");
        System.out.println("3: " + percent3 + "%");
        System.out.println("4: " + percent4 + "%");
        System.out.println("5: " + percent5 + "%");
        System.out.println("6: " + percent6 + "%");
        System.out.println("7: " + percent7 + "%");
        System.out.println("8: " + percent8 + "%");
        System.out.println("9: " + percent9 + "%");
        System.out.println("10: " + percent10 + "%");
        System.out.println("11: " + percent11 + "%");
        System.out.println("12: " + percent12 + "%");


    }

    public static int rollDie(){
        Random r = new Random();
        int value = r.nextInt(6) + 1;
        return value;
    }

    public static double tally(int search, int[] rolls){
        double sum = 0;
        for (int i = 0; i < rolls.length; i++){
            if(search == rolls[i]){
                sum = sum + 1;
            }
        }
        double percent = (sum / rolls.length) * 100;

        return percent;
    }
}
