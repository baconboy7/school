package com.ryan;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Random r = new Random();
        int answer = r.nextInt(100) + 1;

        System.out.println(answer);
        System.out.println("Pick a number between 1 - 100");
        newGuess(answer, 0);
    }

    public static void newGuess(int randomInt, int totalTries){
        Scanner scanner = new Scanner(System.in);
        int answer = randomInt;
        int currentTries = totalTries + 1;

        System.out.print("Enter your guess:");
        int guess = scanner.nextInt();

        validateGuess(guess, answer, currentTries);
    }

    public static void validateGuess(int guess, int answer, int tries) {
        //invalid
        if (guess < 1 || guess > 100) {
            System.out.println("You must choose a number between 1 and 100. Try again");
            int totalTries = tries - 1;
            newGuess(answer, totalTries);

        }

        //too high
        else if (guess > answer) {
            System.out.println("Your guess was to high. Try again");
            newGuess(answer,tries);
        }

        //too low
        else if (guess < answer) {
            System.out.println("Your guess was to low. Try again");
            newGuess(answer,tries);
        }

        //correct
        else if (guess == answer) {
            System.out.println("You win. It took you " + tries + " tries.");
        }

    }
}
