package com.ryan;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);

        System.out.print("Enter the month you were born:");
        String month = scanner.next();

        System.out.print("Enter the day you were born:");
        String day = scanner.next();

        System.out.print("Enter the year you were born:");
        int year = scanner.nextInt();
        int retireAge = year + 67;

        System.out.println("You will retire on " + month + " " + day + ", " + retireAge + ".");
    }
}
