package com.ryan;

public class Motherboard {
    private String model;
    private int ramSlots;
    private String bios;

    //constructor
    public Motherboard(String model, int ramSlots, String bios) {
        this.model = model;
        this.ramSlots = ramSlots;
        this.bios = bios;
    }

    //getters
    public String getModel() {
        return model;
    }

    public int getRamSlots() {
        return ramSlots;
    }

    public String getBios() {
        return bios;
    }

    //load program method
    public void loadProgram(String programName){
        System.out.println(programName + " is loading..");
    }
}
