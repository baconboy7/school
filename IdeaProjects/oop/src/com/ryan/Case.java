package com.ryan;

public class Case {
    private String model;
    private String powerSupply;
    private Dimensions dimensions;

    //Constructor
    public Case(String model, String powerSupply, Dimensions dimensions) {
        this.model = model;
        this.powerSupply = powerSupply;
        this.dimensions = dimensions;
    }

    //Getters
    public String getModel() {
        return model;
    }

    public String getPowerSupply() {
        return powerSupply;
    }

    public Dimensions getDimensions() {
        return dimensions;
    }

    //methods
    public void pressPowerButton() {
        System.out.println("Power Button Pressed");
    }
}
