package com.ryan;

public class Main {

    public static void main(String[] args) {
        //basic oop
//        VipCustomer customer1 = new VipCustomer();
//        VipCustomer customer2 = new VipCustomer("jim", 20);
//        VipCustomer customer3 = new VipCustomer("Bill", 30, "bill@gmail.com");
//
//        System.out.println(customer1.getCustomerName());

        //other oop
//        Case theCase = new Case("The Sweet Case", "Power for days", new Dimensions(20,20,5));
//        Monitor theMonitor = new Monitor("Big Monitor", 27, new Resolution(2540, 1140));
//        Motherboard theMotherboard = new Motherboard("MB-1000", 4,"v.1");
//
//        Computer thePC = new Computer(theCase, theMonitor, theMotherboard);
//        thePC.getTheMonitor().drawPixelAt(100,200,"red");

        //encapsulation
//        Player player = new Player("jim", 50,"Sword");
//        System.out.println("Player name: " + player.getName());
//        System.out.println("Player health: " + player.getHealth());
//
//        player.loseHealth(50);
//        System.out.println("new health: " + player.getHealth());
//
//        Printer printer = new Printer(50, true);
//        printer.printPage(2);
//        printer.printPage(4);

        //polymorphism

    }
}
