package com.ryan;

public class Monitor {
    private String model;
    private int size;
    private Resolution resolution;



    //constructor
    public Monitor(String model, int size, Resolution resolution) {
        this.model = model;
        this.size = size;
        this.resolution = resolution;
    }

    //getters
    public String getModel() {
        return model;
    }

    public int getSize() {
        return size;
    }

    public Resolution getResolution() {
        return resolution;
    }

    //methods
    public void drawPixelAt(int x, int y, String color){
        System.out.println("Drawing pixel at " + x + ',' + y + " in: " + color);
    }
}
