package com.ryan;

public class VipCustomer {
    private String customerName;
    private int creditLimit;
    private String email;

    //empty constructor
    public VipCustomer(){
        this("John Doe", 0, "No Email");
    }

    //name & credit - no email
    public VipCustomer(String customerName, int creditLimit) {
        this(customerName, creditLimit, "No Email");
    }


    //main constructor
    public VipCustomer(String customerName, int creditLimit, String email) {
        this.customerName = customerName;
        this.creditLimit = creditLimit;
        this.email = email;
    }

    //getters
    public String getCustomerName() {
        return customerName;
    }

    public int getCreditLimit() {
        return creditLimit;
    }

    public String getEmail() {
        return email;
    }
}
