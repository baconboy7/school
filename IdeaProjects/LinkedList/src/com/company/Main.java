package com.company;

import java.sql.SQLOutput;
import java.util.*;

public class Main {

    private static ArrayList<Album> albums = new ArrayList<Album>();

    public static void main(String[] args) {


        Album album = new Album("Album 1", "Artist 1");
        album.addSong("Song 1", 4.45);
        album.addSong("Song 2", 2.50);
        album.addSong("Song 3", 2.61);
        album.addSong("Song 4", 5.45);
        albums.add(album);

        Album albumtwo = new Album("Album 2", "Artist 2");
        album.addSong("Song One", 4.45);
        album.addSong("Song Two", 2.50);
        album.addSong("Song Three", 2.61);
        album.addSong("Song Four", 5.45);
        albums.add(albumtwo);


        LinkedList<Song> playlist = new LinkedList<Song>();
        albums.get(0).addToPlaylist(1, playlist);
        albums.get(0).addToPlaylist(3, playlist);
        albums.get(0).addToPlaylist(4, playlist);
//        albums.get(1).addToPlaylist(2, playlist);
//        albums.get(1).addToPlaylist(3, playlist);

        play(playlist);

    }

    private static void play(LinkedList<Song> playlist){
        Scanner scanner = new Scanner(System.in);
        boolean quit = false;
        boolean forward = true;


        ListIterator<Song> listIterator = playlist.listIterator();
        if (playlist.size() == 0){
            System.out.println("No songs in the playlist");
            return;
        } else{
            System.out.println("Now playing " + listIterator.next().getTitle());
            printMenu();
        }

        while (!quit) {
            int action = scanner.nextInt();
            scanner.nextLine();

            switch (action) {
                case 0:
                    System.out.println("Playlist Complete.");
                    quit = true;
                    break;
                case 1:
                    if (!forward) {
                        if (listIterator.hasNext()) {
                            listIterator.next();
                        }
                        forward = true;
                    }
                    if (listIterator.hasNext()) {
                        System.out.println("Now playing " + listIterator.next().getTitle());
                    } else {
                        System.out.println("End of playlist!");
                        forward = false;
                    }
                    break;
                case 2:
                    if (forward) {
                        if (listIterator.hasPrevious()) {
                            listIterator.previous();
                        }
                        forward = false;
                    }
                    if (listIterator.hasPrevious()) {
                        System.out.println("Now playing " + listIterator.previous().getTitle());
                    } else {
                        System.out.println("End of playlist!");
                        forward = true;
                    }
                    break;
                case 3:
                    if (forward){
                        if(listIterator.hasPrevious()){
                            System.out.println("Now replaying " + listIterator.previous().getTitle());
                            forward = false;
                        } else {
                            System.out.println("You are at the start of the playlist!");
                        }
                    } else {
                        if (listIterator.hasNext()){
                            System.out.println("Now replaying " + listIterator.next().getTitle());
                            forward = true;
                        }
                    }
                    break;
                case 4:
                    printlist(playlist);
                    break;
                case 5:
                    printMenu();
                    break;
            }
        }

    }

    private static void printMenu(){
        System.out.println("Menu:");
        System.out.println("1: Next song");
        System.out.println("2: previous song");
        System.out.println("3: replay current song");
        System.out.println("4: list songs in playlist");
        System.out.println("5: show menu");
    }

    private static void printlist(LinkedList<Song> playlist){
        Iterator<Song> iterator = playlist.iterator();
        System.out.println("==========================");
        while(iterator.hasNext()){
            System.out.println(iterator.next().getTitle());
        }
        System.out.println("==========================");
    }



}
