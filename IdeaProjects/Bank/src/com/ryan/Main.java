package com.ryan;

public class Main {

    public static void main(String[] args) {

        Bank bank = new Bank("Bank One");

        bank.addbranch("Branch One");

        bank.addCustomer("Branch One", "Ryan", 12);
        bank.addCustomer("Branch One", "Brad", 120);
        bank.addCustomer("Branch One", "Jake", 50);

    }
}