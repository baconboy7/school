<?
/* App Core Class
    *creates URL
    *Loads Core Controller
    *url Format - /controller/method/params
    */

class Core{
    protected $currentController = 'Pages';
    protected $currentMethod = 'index';
    protected $params = [];
    
    public function __construct(){
       // print_r($this->getUrl());
        
        $url = $this->getUrl();
        
        //look in Controllers for First Value
        if(file_exists('../app/Controllers/' . ucwords($url[0]) . '.php')){
            //if exists then set current controller
            $this->currentController = ucwords($url[0]);
            //unset 0 index
            unset($url[0]);
        }
        
        //require the controller
        require_once '../app/Controllers/' . $this->currentController . '.php';
        
        //instantiate controller
        $this->currentController = new $this->currentController;
        
        
        //check for second part of url
        if (isset($url[1])){
            //check for method in controller
            if(method_exists($this->currentController, $url[1])){
                $this->currentMethod = $url[1];
                //unset 1 index
                unset($url[1]);
            }
            
        }
        
        //get prarams
        $this->params = $url ? array_values($url) : [];
        
        //callback with array
        call_user_func_array([$this->currentController, $this->currentMethod], $this->params);
    }
    
    public function getUrl(){
        if (isset($_GET['url'])){
            $url = rtrim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);
            return $url;
        }
    }
}