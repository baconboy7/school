<?
class Post{
    
    private $db;
    
    public function __construct(){
        $this->db = new Database;
    }
    
    public function getPosts(){
        $this->db->query('SELECT *,
                            postums.posts.id as postID,
                            postums.users.id as userID,
                            postums.posts.createdAt as postCreated
                            FROM postums.posts
                            INNER JOIN postums.users
                            ON postums.posts.userID = postums.users.id
                            order by postums.posts.createdAt DESC'
                            );
        
        $results = $this->db->resultSet();
        
        return $results;
    }
    
    public function addPost($data){
        $this->db->query('INSERT INTO postums.posts (title, userid, body, createdAt) VALUES (:title, :userid, :body, NOW());');
        $this->db->bind(':title', $data['title']);
        $this->db->bind(':userid', $data['userid']);
        $this->db->bind(':body', $data['body']);
        
        if ($this->db->execute()){
            return true;
        } else{
            return false;
        }
    }
    
    public function editPost($data){
        $this->db->query('UPDATE postums.posts SET title = :title, body = :body, createdAt = NOW() WHERE id = :id;');
        $this->db->bind(':title', $data['title']);
        $this->db->bind(':body', $data['body']);
        $this->db->bind(':id', $data['id']);
        
        if ($this->db->execute()){
            return true;
        } else{
            return false;
        }
    }
    
    public function getPost($id){
        $this->db->query('SELECT * FROM postums.posts WHERE id = :id');
        $this->db->bind(':id', $id);
        
        $results = $this->db->single();
        
        return $results;
    }
    
      public function deletePost($id){
        $this->db->query('DELETE FROM postums.posts WHERE id = :id');
        $this->db->bind(':id', $id);
        
        $this->db->execute();
        
        return $results;
      }
}