<?
    class Users extends Controller {
        
        public function __construct(){
            $this->userModel = $this->model('User');
        }
        
        public function register(){
           //check if it is post
            if ($_SERVER['REQUEST_METHOD'] == 'POST'){  
                $data = [
                    'title' => 'Register',
                    'name' => trim($_POST['name']),
                    'email' => trim($_POST['email']),
                    'password' => trim($_POST['password']),
                    'passwordConfirm' => trim($_post['passwordConfirm']),
                    'nameError' => '',
                    'emailError' => '',
                    'passwordError' => '',
                    'passwordConfirmError' => ''
                ];
                
                if (empty($data['name'])){
                    $data['nameError'] = 'Please enter a name!';
                }
                    
                if (empty($data['email'])){
                    $data['emailError'] = 'Please enter an email address!';
                } 
                    //check if email is used
                if ($this->userModel->findUserByEmail($data['email'])){
                        $data['emailError'] = 'This email is already taken!';
                    }
                    
                if (empty($data['password'])){
                    $data['passwordError'] = 'Please enter a password!';
                }
                
                if (empty($data['confirmPassword'])){
                    $data['confirmPasswordError'] = 'Please enter a password!';
                }
                    
                if ($data['password'] != $data['confirmPassword']){
                    $data['confirmPasswordError'] = 'Passwords do not match!';
                }
                    
                if (empty($data['nameError']) && empty($data['emailError']) && empty($data['passwordError']) && empty($data['passwordConfirmError'])){
                        //hash password
                    $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
                        //sign up
                    if($this->userModel->register($data)){
                        flash('register_success','You are signed up');
                        redirect('users/login');
                        
                    }else{
                        die("Failed to Sign up.");
                    }
                        
                    
                } else{
                    $this->view('users/register', $data);
                }
                
            } else {
                
                $data = [
                    'title' => 'Register',
                    'name' => '',
                    'email' => '',
                    'password' => '',
                    'passwordConfirm' => '',
                    'nameError' => '',
                    'emailError' => '',
                    'passwordError' => '',
                    'passwordConfirmError' => ''
                ];
                
                //load view
                $this->view('users/register', $data);
            }
            
        }
        
         public function login(){
              //check if it is post
            if ($_SERVER['REQUEST_METHOD'] == 'POST'){  
                $data = [
                    'title' => 'Login',
                    'email' => trim($_POST['email']),
                    'password' => trim($_POST['password']),
                    'emailError' => '',
                    'passwordError' => ''
                ];
                    
                if (empty($data['email'])){
                    $data['emailError'] = 'Please enter an email address!';
                }
                    
                if (empty($data['password'])){
                    $data['passwordError'] = 'Please enter a password!';
                }
                
                if ($this->userModel->findUserByEmail($data['email']) == false){
                    $data['emailError'] = 'Email does not exist';
                }
                    
                if (empty($data['emailError']) && empty($data['passwordError'])){
                    $loggedInUser = $this->userModel->login($data['email'], $data['password']);
                    
                    if ($loggedInUser){
                        $this->createUserSession($loggedInUser);
                    } else{
                        $data ['passwordError'] = 'Password incorrect';
                         $this->view('users/login', $data);
                    }
                    
                    
                } else{
                    $this->view('users/login', $data);
                }
                
            } else {
                
                 
                $data = [
                    'title' => 'Login',
                    'email' => '',
                    'password' => '',
                    'emailError' => '',
                    'passwordError' => ''
                ];
                
                //load view
                $this->view('users/login', $data);
                
            }
            
         }
        public function createUserSession($user){
            
            $_SESSION['userID'] = $user->id;
            
            $_SESSION['userEmail'] = $user->email;
            $_SESSION['userName'] = $user->name;
            redirect('posts/index');
        }
        
        public function logout(){
            session_destroy();
            redirect('users/login');
        }
        public function isLoggedIn(){
            if(isset($_SESSION['userID'])){
                return true;
            } else{
                return false;
            }
        }
    }