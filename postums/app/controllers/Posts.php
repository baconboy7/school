<?

class Posts extends Controller{
    
    public function __construct(){
        if(!isset($_SESSION['userID'])){
            redirect('users/login');
        }
        
        $this->postModel = $this->model('Post');
    }
    
    public function index(){
        $posts = $this->postModel->getPosts();
        
        $data = [
            'posts' => $posts
        ];
        
        $this->view('posts/index', $data);
        
    }
    
    public function add(){
        if($_SERVER['REQUEST_METHOD']== 'POST'){
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            
            $data = [
                'title' => trim($_POST['title']),
                'body' => trim($_POST['body']),
                'userid' => $_SESSION['userID'],
                'titleError' => '',
                'bodyError' => ''
        ];
            //validate data
            if(empty($data['title'])){
                $data['titleError'] = 'Please enter a title';
            }
            if(empty($data['body'])){
                $data['bodyError'] = 'Please enter a post';
            }
            
            //check for errors
            if(empty($data['titleError']) && empty($data['bodyError'])){
                if($this->postModel->addPost($data)){
                    flash('postMessage', "Post Added");
                        redirect('posts');
                }else{
                    die("Shit broke");
                }
            } else {
                $this->view('posts/add', $data);
            }
        }
        else{
            $data = [
            'title' => '',
            'body' => ''
        ];
        }
        
    $this->view('posts/add', $data);

    }
    public function edit($id){
        
        if($_SERVER['REQUEST_METHOD']== 'GET'){
            
          $results =  $this->postModel->getPost($id);
           
            
            $data = [
                'title' => $results->title,
                'body' => $results->body,
                'id' => $id,
                'titleError' => '',
                'bodyError' => ''
                ];
            $this->view('posts/edit', $data);
        }
        
        
        if($_SERVER['REQUEST_METHOD']== 'POST'){
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            
            $data = [
                'title' => trim($_POST['title']),
                'body' => trim($_POST['body']),
                'id' =>trim($_POST['id']),
                'titleError' => '',
                'bodyError' => ''
        ];
            print_r($data);
            //validate data
            if(empty($data['title'])){
                $data['titleError'] = 'Please enter a title';
            }
            if(empty($data['body'])){
                $data['bodyError'] = 'Please enter a post';
            }
            
            //check for errors
            if(empty($data['titleError']) && empty($data['bodyError'])){
                if($this->postModel->editPost($data)){
                    flash('postMessage', "Post Updated");
                        redirect('posts');
                }else{
                    die("Shit broke");
                }
            } else {
                $this->view('posts/edit', $data);
            }
        }
        else{
            $data = [
            'title' => '',
            'body' => ''
        ];
        }
        
    $this->view('posts/edit', $data);

    }
     public function delete($id){
        
        if($_SERVER['REQUEST_METHOD']== 'GET'){
            
          $results =  $this->postModel->deletePost($id);
            flash('postMessage', "Post Deleted");
                        redirect('posts');
        }
     }
}