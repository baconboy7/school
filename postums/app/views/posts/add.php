<? require APPROOT . '/views/inc/header.php'; ?>


<a href="../index.php" class="btn btn-light mt-2">Back</a>
    <div class="card card-body bg-light mt-3">
        <h3>Add Post</h3>
        
        <form action="<? echo URLROOT; ?>/posts/add" method="post">
            
            <div class="form-group">
                <label for="title">Title: </label>
                <input type="text" name="title" class="form-control form-control-lg <? echo (!empty($data['titleError'])) ? 'is-invalid' : '' ?>" value="<? echo $data['title']; ?>">
                <span class="invalid-feedback"><? echo $data['titleError']; ?></span>
            </div>
            
            <div class="form-group">
                <label for="body">Body: </label>
                <textarea name="body" class="form-control form-control-lg <? echo (!empty($data['bodyError'])) ? 'is-invalid' : '' ?> "><? echo $data['body']; ?></textarea>
                <span class="invalid-feedback"><? echo $data['bodyError']; ?></span>
            </div>
            <input type="submit" value="Submit" class="btn btn-success col-md-2">
        </form>
        
    </div>


<? require APPROOT . '/views/inc/footer.php'; ?>