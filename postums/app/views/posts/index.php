<? require APPROOT . '/views/inc/header.php'; ?>
<? flash('postMessage'); ?>
    <div class="row p-3">
        <div class='col-md-10'>
            <h2>Posts</h2>        </div>
        <div class='col-md-1'>
            <a href="<? echo URLROOT; ?>/posts/add" class="btn btn-primary">Add Post</a>                                           
        </div>
    </div>
<? foreach($data['posts'] as $post) : ?>
    <div class = 'card card-body mb-3'>
        <div class = 'row'>
            <h4 class="card-title col-md-10 col-sm-8"><? echo $post->title; ?></h4>
            <? if($post->id == $_SESSION['userID']) : ?>
                <a href="<? echo URLROOT; ?>/posts/edit/<? echo $post->postID; ?>" class="col-md-1 btn btn-light m-2" >Edit</a>
                <a href=" <? echo URLROOT; ?>/posts/delete/<? echo $post->postID; ?>" class="btn btn-dark m-2">X</a>
            <? endif ?>
        </div>
        <p class=" bg-light p-2 mb-3">Written by: <? echo $post->name; ?> on <? echo $post->postCreated; ?></p>
        <p class="card-text bg-light p-2 mb-3"><? echo $post->body; ?></p>
       
    </div>

<? endforeach; ?>

<? require APPROOT . '/views/inc/footer.php'; ?>