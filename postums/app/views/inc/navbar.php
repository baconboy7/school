<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="<? echo URLROOT ?>"><? echo SITENAME ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<? echo URLROOT ?>">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<? echo URLROOT ?>/pages/about">About</a>
      </li>
      </ul>
      <ul class="navbar-nav ml-auto">
      <? if(isset($_SESSION['userID'])) : ?>
      
          <li class="nav-item">
            <a class="nav-link" href="<? echo URLROOT ?>/users/logout">Logout</a>
          </li>
    <? else : ?>
          <li class="nav-item">
            <a class="nav-link" href="<? echo URLROOT ?>/users/register">Register</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<? echo URLROOT ?>/users/login">Login</a>
          </li>
    <? endif; ?>
      </ul>
    </div>
</nav>