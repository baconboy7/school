<html>
    <head>
        <title><? echo SITENAME ?></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel=stylesheet type="text/css" href="<? echo URLROOT ?>/css/style.css"> 
    </head>

<body>
<? require APPROOT . '/views/inc/navbar.php'; ?>
    
    <div class="container-fluid">
        <div class="container">