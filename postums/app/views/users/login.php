<? require APPROOT . '/views/inc/header.php'; ?>


<div class="col-md-6 mx-auto">
    <div class="card card-body bg-light mt-5">
        <h3>Login</h3>
        <? echo flash('register_success'); ?>
        <form action="<? echo URLROOT; ?>/users/login" method="post">
            
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" name="email" class="form-control form-control-lg <? echo (!empty($data['emailError'])) ? 'is-invalid' : '' ?>" value="<? echo $data['email']; ?>">
                <span class="invalid-feedback"><? echo $data['emailError']; ?></span>
            </div>
            
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" name="password" class="form-control form-control-lg <? echo (!empty($data['passwordError'])) ? 'is-invalid' : '' ?>" value="<? echo $data['password']; ?>">
                <span class="invalid-feedback"><? echo $data['passwordError']; ?></span>
            </div>
            
            <div class="row">
                <div class="col">
                    <input type="submit" value="Login" class="btn btn-success btn-block">
                </div>
                <div class="col">
                    <a href="<? echo URLROOT ?>/users/register" class="btn btn-light btn-block">Need an account? Register.</a>
                
                </div>
            </div>
        </form>
        
    </div>
</div>

<? require APPROOT . '/views/inc/footer.php'; ?>