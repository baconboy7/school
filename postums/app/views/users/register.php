<? require APPROOT . '/views/inc/header.php'; ?>


<div class="col-md-6 mx-auto">
    <div class="card card-body bg-light mt-5">
        <h3>Create an Account</h3>
       
        <form action="<? echo URLROOT; ?>/users/register" method="post">
            
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" name="name" class="form-control form-control-lg <? echo (!empty($data['nameError'])) ? 'is-invalid' : '' ?>" value="<? echo $data['name']; ?>">
                <span class="invalid-feedback"><? echo $data['nameError']; ?></span>
            </div>
            
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" name="email" class="form-control form-control-lg <? echo (!empty($data['emailError'])) ? 'is-invalid' : ''; ?>" value="<? echo $data['email']; ?>">
                <span class="invalid-feedback"><? echo $data['emailError']; ?></span>
            </div>
            
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" name="password" class="form-control form-control-lg <? echo (!empty($data['passwordError'])) ? 'is-invalid' : '' ?>" value="<? echo $data['password']; ?>">
                <span class="invalid-feedback"><? echo $data['passwordError']; ?></span>
            </div>
            
            <div class="form-group">
                <label for="confirmPassword">Confirm Password:</label>
                <input type="password" name="confirmPassword" class="form-control form-control-lg <? echo (!empty($data['confirmPasswordError'])) ? 'is-invalid' : '' ?>" value="<? echo $data['confirmPassword']; ?>">
                <span class="invalid-feedback"><? echo $data['confirmPasswordError']; ?></span>
            </div>
            
            <div class="row">
                <div class="col">
                    <input type="submit" value="Register" class="btn btn-success btn-block">
                </div>
                <div class="col">
                    <a href="<? echo URLROOT ?>/users/login" class="btn btn-light btn-block">Have an account? Login.</a>
                </div>
            </div>
        </form>
        
    </div>
</div>

<? require APPROOT . '/views/inc/footer.php'; ?>